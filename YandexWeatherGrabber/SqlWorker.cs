﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using YandexWeatherGrabber.Properties;

namespace YandexWeatherGrabber
{
    public sealed class SqlWorker
    {
        private SqlCeConnection _sqlConn;
        private SqlCeDataAdapter _sqlAdapt = new SqlCeDataAdapter();
        private SqlCeCommand _sqlCmd;

        public SqlWorker()
        {
            //подгружаем connectionString
            UpdateSqlInfo();
        }

        public void UpdateSqlInfo()
        {
            _sqlConn = new SqlCeConnection(Settings.Default.connectionString);
        }

        public DataTable GeneralSqlRequest(string columns, string table, string options)
        {
            string requestString = $"SELECT {columns} FROM {table} {options.Trim()}";

            _sqlConn.Open();
            _sqlCmd = new SqlCeCommand(requestString) {Connection = _sqlConn};
            _sqlAdapt = new SqlCeDataAdapter(_sqlCmd);
            DataTable dt = new DataTable();
            _sqlAdapt.Fill(dt);
            _sqlConn.Close();

            return dt;
        }

        public void GeneralSelectSqlRequest(string columns, string values)
        {
            string requestString = "INSERT INTO [MainDates] " +
                                   $"({columns}) " +
                                   $"VALUES ({values})";




            //string requestString = "INSERT INTO [MainDates] ([Date],[ToADay],[Yesterday],[2DaysAgo] ,[3DaysAgo],[4DaysAgo],[5DaysAgo],[6DaysAgo],[7DaysAgo],[8DaysAgo],[9DaysAgo],[lastUpdate]) VALUES (GetDate()-1,NULL,NULL,NULL,NULL,NULL ,NULL,NULL ,NULL,NULL,NULL ,NULL) GO";

            _sqlConn.Open();
            _sqlCmd = new SqlCeCommand(requestString) {Connection = _sqlConn};
            //_sqlCmd.Parameters.AddWithValue("@values", values);
            _sqlCmd.ExecuteNonQuery();
            _sqlConn.Close();
        }
    }
}
