﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;

namespace YandexWeatherGrabber
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //урлы для xml-api файлов
        private const string URL_ZHUK = @"http://export.yandex.ru/weather-ng/forecasts/27626.xml";
        //private const string urlSamara = @"http://export.yandex.ru/weather-ng/forecasts/28807.xml";
        //private const string urlOtrad = @"http://export.yandex.ru/weather-ng/forecasts/28808.xml";

        SqlWorker _sqlWorker = new SqlWorker();
        XmlWorker _xmlWorker = new XmlWorker();

        public MainWindow()
        {
            InitializeComponent();
            UpdateMainListView();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            GlobalUpdate();
        }

        public void UpdateMainListView()
        {
            DataTable dt = _sqlWorker.GeneralSqlRequest("Date", "MainDates", "order by Date desc");
            listView.ItemsSource = dt.DefaultView;
        }
        
        public void GlobalUpdate()
        {
            // ЧТО НУЖНО ДЕЛАТЬ
            //
            // 1 условие: Последняя дата апдейта была больше, чем 3 часа назад
            //
            // 2 условие: Сверять, чтобы в таблице были дни: СЕГОДНЯ + 9 следующих
            //
            // TODO: В каждую строку начиная с сегодняшнего дня добавить прогноз (условие - если там не пусто)
            // реализовать как метод, который в зависимости от сегодняшней даты 
            // считает сколько за сколько дней ставится прогноз.
            //
            // 

            //_sqlWorker.GeneralSelectSqlRequest(DateTime.Now.ToString());

            // проверка на последнюю дату и добавление новых
            DataTable dt = _sqlWorker.GeneralSqlRequest("top 1 *", "MainDates", "order by id desc");
            DateTime lastDate = DateTime.Parse(dt.Rows[0][1].ToString());
            int days = DateTime.Now.Subtract(lastDate).Days;
            if (days > -9)
            {
                for (int i = days; i > -9; i--)
                    _sqlWorker.GeneralSelectSqlRequest("[Date],[ToADay],[Yesterday],[2DaysAgo],[3DaysAgo],[4DaysAgo],[5DaysAgo],[6DaysAgo],[7DaysAgo],[8DaysAgo],[9DaysAgo],[lastUpdate]",
                        $"GetDate()-{i}-1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL");
            }

            // посмотреть под брейкпойнтом почему не работает geleralselectsqlrequest


            //в конце апдейт основного листфью
            UpdateMainListView();
        }
    }
}
